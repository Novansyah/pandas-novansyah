#!/usr/bin/env python
# coding: utf-8

# In[2]:


import numpy as np
import pandas as pd


# In[8]:


labels = ['a','b','c']
my_list = [10,20,30]
arr = np.array([10,20,30])
d = {'a':10, 'b':20, 'c':30}


# In[9]:


pd.Series(data=my_list)


# In[10]:


pd.Series(data = my_list, index = labels)


# In[11]:


pd.Series(my_list, labels)


# In[12]:


pd.Series(data=labels)


# In[13]:


pd.Series([sum,print,len])


# In[15]:


ser1 = pd.Series([1,2,3,4], index = ['USA','Germany','USSR','Japan'])
ser1


# In[16]:


ser2 = pd.Series([1,2,5,4], index = ['USA','Germany','Italy','Japan'])
ser2


# In[18]:


ser1['USA']


# In[19]:


ser1 + ser2


# In[20]:


from numpy.random import randn
np.random.seed(101)
df = pd.DataFrame(randn(5,4), index = 'A B C D E'.split(), columns='W X Y Z'.split())
df


# In[21]:


df['W']


# In[22]:


df[['W','Z']]


# In[23]:


df.W


# In[24]:


df['new'] = df['W'] + df['Y']
df


# In[25]:


df.drop('new', axis=1)


# In[26]:


df


# In[27]:


df.drop('new', axis=1, inplace=True)
df


# In[28]:


df.drop('E',axis=0)


# In[29]:


df.loc['A']


# In[30]:


df.iloc[2]


# In[31]:


df.loc['B','Y']


# In[32]:


df.loc[['A','B'], ['W','Y']]


# In[33]:


df


# In[34]:


df > 0


# In[35]:


df[df>0]


# In[36]:


df[df['W']>0]


# In[37]:


df[df['W']>0]['Y']


# In[38]:


df[df['W']>0][['Y','X']]


# In[39]:


df[(df['W']>0)&(df['Y']>1)]


# In[40]:


df


# In[41]:


df.reset_index()


# In[42]:


newind = 'CA NY WY OR CO'.split()
df['States'] = newind
df


# In[43]:


df.set_index('States')


# In[44]:


df


# In[45]:


df.set_index('States', inplace=True)
df


# In[46]:


outside = ['G1','G1','G1','G2','G2','G2']
inside = [1,2,3,1,2,3]
hier_index = list(zip(outside,inside))
hier_index = pd.MultiIndex.from_tuples(hier_index)
hier_index


# In[47]:


df = pd.DataFrame(np.random.randn(6,2), index=hier_index, columns=['A','B'])
df


# In[48]:


df.loc['G1']


# In[49]:


df.loc['G1'].loc[1]


# In[50]:


df.index.names


# In[51]:


df.index.names = ['Group','Num']
df


# In[52]:


df.xs('G1')


# In[53]:


df.xs(['G1',1])


# In[54]:


df.xs(1,level='Num')


# In[55]:


df = pd.DataFrame({'A':[1,2,np.nan],'B':[5,np.nan, np.nan],'C':[1,2,3]})
df


# In[56]:


df.dropna()


# In[57]:


df.dropna(axis=1)


# In[58]:


df.dropna(thresh=2)


# In[59]:


df.fillna(value='FILL VALUE')


# In[60]:


df['A'].fillna(value=df['A'].mean())


# In[62]:


data = {'Company':['GOOG','GOOG','MSFT','MSFT','FB','FB'], 'Person':['Sam','Charlie','Amy','Vanessa','Carl','Sarah'], 'Sales':[200,120,340,124,243,350]}
df = pd.DataFrame(data)
df


# In[63]:


df.groupby('Company')


# In[64]:


by_comp = df.groupby("Company")


# In[65]:


by_comp.mean()


# In[66]:


df.groupby('Company').mean()


# In[67]:


by_comp.std()


# In[68]:


by_comp.min()


# In[69]:


by_comp.max()


# In[70]:


by_comp.count()


# In[71]:


by_comp.describe()


# In[72]:


by_comp.describe().transpose()


# In[73]:


by_comp.describe().transpose()['GOOG']


# In[77]:


import pandas as pd


# In[81]:


left = pd.DataFrame({'key':['K0','K1','K2','K3'],
                    'A':['K0','K1','K2','K3'],
                    'B':['B0','B1','B2','B3']})
right = pd.DataFrame({'key':['K0','K1','K2','K3'],
                    'C':['C0','C1','C2','C3'],
                    'D':['D0','D1','D2','D3']})


# In[82]:


left


# In[83]:


right


# In[84]:


pd.merge(left,right,how='inner',on='key')


# In[97]:


left = pd.DataFrame({'key1':['K0','K0','K2','K3'],
                     'key2' :['K0','K1','K0','K1'],
                    'A':['K0','K1','K2','K3'],
                    'B':['B0','B1','B2','B3']})
right = pd.DataFrame({'key1':['K0','K1','K1','K3'],
                    'key2' :['K0','K0','K0','K0'],
                    'C':['C0','C1','C2','C3'],
                    'D':['D0','D1','D2','D3']})


# In[98]:


pd.merge(left,right,on=['key1','key2'])


# In[99]:


pd.merge(left,right,how='outer', on=['key1','key2'])


# In[100]:


pd.merge(left,right,how='right', on=['key1','key2'])


# In[101]:


pd.merge(left,right,how='left', on=['key1','key2'])


# In[110]:


left = pd.DataFrame({'A':['A0','A1','A2'],
                    'B':['B0','B1','B2']},
                   index= ['K0','K1','K2'])
right = pd.DataFrame({'C':['C0','C2','C3'],
                    'D':['D0','D2','D3']},
                   index= ['K0','K2','K3'])


# In[111]:


left.join(right)


# In[112]:


left.join(right, how='outer')


# In[113]:


import pandas as pd
df = pd.DataFrame({'col1':[1,2,3,4],'col2':[444,555,666,444],'col3':['abc','def','ghi','xyz']})
df.head()


# In[114]:


df['col2'].unique()


# In[115]:


df['col2'].nunique()


# In[116]:


df['col2'].value_counts()


# In[117]:


newdf = df[(df['col1']>2)&(df['col2']==444)]
newdf


# In[118]:


def times2(x):
    return x*2


# In[120]:


df['col1'].apply(times2)


# In[121]:


df['col3'].apply(len)


# In[123]:


df['col1'].sum()


# In[124]:


del df['col1']
df


# In[125]:


df.columns


# In[126]:


df.index


# In[127]:


df


# In[128]:


df.sort_values(by='col2')


# In[129]:


df.isnull()


# In[130]:


df.dropna()


# In[131]:


import numpy as np


# In[132]:


df = pd.DataFrame({'col1':[1,2,3,np.nan],
                  'col2':[np.nan,555,666,444],
                  'col3':['abc','def','ghi','xyz']})
df.head()


# In[133]:


df.fillna('FILL')


# In[137]:


import pandas as pd
data = {'A':['foo','foo','foo','bar','bar','bar'],
       'B':['one','one','two','two','one','one'],
       'C':['x','y','x','y','x','y'],
       'D': [1,3,2,5,4,1]}
df = pd.DataFrame(data)
df


# In[138]:


df.pivot_table(values='D',index=['A','B'],columns=['C'])


# In[139]:


import numpy as np
import pandas as pd


# In[143]:


df = pd.read_csv('example')
df


# In[148]:


df.to_csv('example',index=False)


# In[153]:


pip install xlrd


# In[155]:


pd.read_excel('Excel_Sample.xlsx', sheet_name='Sheet1')


# In[157]:


pip install lxml


# In[158]:


pip install html5lib


# In[159]:


pip install BeautifulSoup4


# In[161]:


df = pd.read_html('http://www.fdic.gov/bank/individual/failed/banklist.html')
df[0]


# In[ ]:




