#!/usr/bin/env python
# coding: utf-8

# In[24]:


import numpy as np
import pandas as pd


# In[30]:


banks = pd.read_csv('banklist.csv')
banks


# In[31]:


banks.head(5)


# In[32]:


banks.columns


# In[33]:


banks['ST'].nunique()


# In[35]:


arr = np.array(banks['ST'].unique())
arr


# In[36]:


bank = banks['ST'].value_counts().iloc[0:5]
bank.index.name = 'ST'
bank


# In[37]:


bank = banks['Acquiring Institution'].value_counts().iloc[0:5]
bank


# In[38]:


banks[banks['Acquiring Institution']=='State Bank of Texas']


# In[39]:


data = banks[banks['City']=='Los Angeles']
newdata = data.groupby('City')
newdata.count()


# In[40]:


sub = 'Bank'

data = banks[~banks['Bank Name'].str.contains(sub)]
len(data.index)


# In[41]:


sub = 'S'

data = banks['Bank Name'].astype(str).str[0]
newdata = data[data=='S']
len(newdata)


# In[42]:


newdata = banks[banks['CERT']>20000]
len(newdata)


# In[43]:


data = banks['Bank Name'].str.count(' ')
newdata = data[data==1]
len(newdata)


# In[44]:


import datetime
import pandas as pd 

banks['year'] = pd.DatetimeIndex(banks['Closing Date']).year
data = banks[banks['year']==2008]
len(data)


# In[ ]:




